*******************************************************
      README.txt for userlists.module for Drupal
     by Jeff Robbins :: jeff /@t\ jjeff /d0t\ com
*******************************************************

Hardly a full fledged module, this module adds a tab
entitled 'by role' on the admin/user page. This tab
provides a sortable list of the site users by role. It
is a quick way to confirm which users are administrators,
editors, or whatever roles you have defined.


INSTALLATION:

Put the module in your modules directory.
Enable it.
Done.